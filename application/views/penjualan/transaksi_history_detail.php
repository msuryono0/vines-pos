<?php
$kasir = $data_order->KasirName;
if(!empty($data_order)){
	echo "
		<table class='info_pelanggan'>
			<tr>
				<td>Nama Pelanggan</td>
				<td>:</td>
				<td>".$data_order->sellToCustomerName."</td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td>Jakarta</td>
			</tr>
			<tr>
				<td>Telp. / HP</td>
				<td>:</td>
				<td>".$data_order->NoTelpPelanggan."</td>
			</tr>
			
		</table>
		<hr />
		<input type='hidden' id='TglPrint' value='".date('d-m-Y H:i:s', strtotime($data_order->PostingDate))."' />
		<input type='hidden' id='NotaPrint' value='".$data_order->no."' />
		<input type='hidden' id='kasirName' value='".$data_order->KasirName."' />

	";
}

?>

<table id="my-grid" class="table tabel-transaksi" style='margin-bottom: 0px; margin-top: 10px;'>
	<thead>
		<tr>
			<th>#</th>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Harga Satuan</th>
			<th>Jumlah Beli</th>
			<th>Sub Total</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no 			= 1;
	$totalbayar = 0;
	$stringPayment = "";
	$stringdetail = "";
	$potongan = 0;
	foreach($detail as $d){
		$subTotal = $d->unitPrice * $d->quantity;
		echo "<tr>
				<td>".$no."</td>
				<td>".$d->no."</td>
				<td>".$d->description."</td>
				<td align=''>".number_format($d->unitPrice,2,',','.')."</td>
				<td align=''>".$d->quantity."</td>
				<td align=''>".number_format($subTotal,2,',','.')."</td>
			</tr>";
		$no++;
		$totalbayar = $totalbayar + $subTotal;
		$potongan = $potongan + $d->DiscountAmount;
		$stringdetail = $stringdetail . '@' . $d->description . '#' . $d->quantity . '#' . str_replace(',', '.',number_format($d->unitPrice)) . '#' . str_replace(',', '.',number_format($potongan))  . '#' . str_replace(',', '.',number_format($subTotal));
	}

	$dv = 0;
	foreach($payment as $dev){			
		$dv = isset($dev->DeliveryOrder) ? $dev->DeliveryOrder : 0;
	}

	echo "	
		<tr style='background:#deeffc;'>
			<td colspan='5' style='text-align:right;'><b>Total</b></td>
			<td><b>Rp. ".number_format($totalbayar,2,',','.')."</b></td>
		</tr>
		<tr style='background:#deeffc;'>
			<td colspan='5' style='text-align:right;'><b>Potongan</b></td>
			<td><b>Rp. ".number_format(($potongan) * -1 ,2,',','.')."</b></td>
		</tr>";
	if ($data_order->KasirName == "ECOM"){ 
		echo "<tr style='background:#deeffc;'>
			<td colspan='5' style='text-align:right;'><b>Delivery</b></td>
			<td><b>Rp. ".number_format($dv,2,',','.')."</b></td>
		</tr>";
	}
	echo "	<tr style='background:#deeffc;'>
			<td colspan='5' style='text-align:right;'><b>Grand Total</b></td>
			<td><b>Rp. ".number_format($totalbayar - $potongan,2,',','.')."</b></td>
		</tr>";

		$totalPayment = 0;
	foreach($payment as $dp){
		$totalPayment = $totalPayment + $dp->NominalPayment;
		echo "<tr style='background:#deeffc;'>";
		echo "<td colspan='5' style='text-align:right;'><b>".$dp->PaymentType."</b></td>";
		echo "<td><b>Rp. ".number_format($dp->NominalPayment,2,',','.')."</b></td>";
		echo "</tr>";
		$stringPayment = $stringPayment . '@' . $dp->PaymentType . '#' . str_replace(',', '.',number_format($dp->NominalPayment));
	}
	echo "<tr style='background:#deeffc;'>";
	echo '<td colspan="5" style="text-align:right;"><b>Kembalian</b></td>';
	echo '<td><b>Rp. '.number_format($totalPayment - ($totalbayar - $potongan),2,',','.').'</b></td>';
	echo '</tr>';
	echo '<tr style="display:none">';
	echo '<td colspan="6" align="right">
			<input type="text" id="DetailProductPrint" value="'. $stringdetail .'" /> 
			<input type="text" id="SubtotalPrint" value="'. str_replace(',', '.',number_format($totalbayar)) .'" /> 
			<input type="text" id="PotonganPrint" value="'.str_replace(',', '.',number_format(($potongan) * -1)).'" />
			<input type="text" id="GrandTotalPrint" value="'. str_replace(',', '.',number_format($totalbayar - $potongan)) .'" />
			<input type="text" id="Kembalian" value="'. str_replace(',', '.',number_format($totalPayment - ($totalbayar - $potongan))) .'" />
			<input type="text" id="PaymentPrint" value="'. $stringPayment .'" />
			<input type="text" id="DeliveryOrder" value="'. str_replace(',', '.',number_format($dv)) .'" />
			</td>';
	echo '</tr>';
	?>
	</tbody>
</table>

<script>
$(document).ready(function(){
	//var Tombol = "<button type='button' class='btn btn-primary' id='Cetaks'><i class='fa fa-print'></i> Cetak</button>";
	var Tombol = "";
	 Tombol += "<button class='btn btn-success' onClick='showReceiptTermal();' id='btn_print'><i class='glyphicon glyphicon-print'></i> Print</button>&nbsp;<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$('button#Cetaks').click(function(){
		var FormData = "nomor_nota="+encodeURI($('#nomor_nota').val());
		FormData += "&tanggal="+encodeURI($('#tanggal').val());
		FormData += "&id_kasir="+$('#id_kasir').val();
		FormData += "&id_pelanggan="+$('#id_pelanggan').val();
		FormData += "&" + $('.tabel-transaksi tbody input').serialize();
		FormData += "&cash="+$('#UangCash').val();
		FormData += "&catatan="+encodeURI($('#catatan').val());
		FormData += "&grand_total="+$('#TotalBayarHidden').val();
		FormData += "&dev_order="+$('#DeliveryOrder').val();

		window.open("<?php echo site_url('penjualan/transaksi-cetak/?'); ?>" + FormData,'_blank');
	});
});
var countPrint = 1;
    function showReceiptTermal() {
		var ee = $("#StoreName").val();
		var printerName = "<?php echo $this->session->userdata('ap_store_printer');?>";
		var hasil = ee.split('&');
		var stringResult = $("#StoreAddress").val() + "&" +
						$("#StoreCity").val() + "&" +
						$("#StorePostCode").val() + "&" +
						$("#StoreHP").val() + "&" +
						$("#TglPrint").val() + "&" +
						$("#NotaPrint").val() + "&" +
						hasil[0] + "&" +
						$("#DetailProductPrint").val() + "&" +
						$("#SubtotalPrint").val() + "&" +
						$("#PotonganPrint").val() + "&" +
						$("#GrandTotalPrint").val() + "&" +
						$("#PaymentPrint").val() + "&" +
						$("#Kembalian").val() + "&1&" + countPrint + "&0878-8338-1818.&" +  hasil[1] + "&" +
						$("#kasirName").val() + "&" + $("#DeliveryOrder").val();
						console.log(stringResult);
        Android.showReceipt(stringResult, printerName);
		countPrint = countPrint + 1;
    }
</script>